#!/usr/bin/env bash

####Deploiement de la machine dwikiorg###

himage dwikiorg mkdir -p /etc/named 
hcp dwikiorg/* dwikiorg:/etc/named/.
hcp dwikiorg/named.conf dwikiorg:/etc/.

###Deploiement de la machine diutre###

himage diutre mkdir -p /etc/named
hcp diutre/* diutre:/etc/named/.
hcp diutre/named.conf diutre:/etc/.

###Deploiement de la machine dorg###

himage dorg mkdir -p /etc/named
hcp dorg/* dorg:/etc/named/.
hcp dorg/named.conf dorg:/etc/.

###Deploiement de la machine aRootServer###

himage aRootServer mkdir -p /etc/named
hcp aRootServer/* aRootServer:/etc/named/.
hcp aRootServer/named.conf aRootServer:/etc/.

###Deploiement de la machine bRootServer###
himage bRootServer mkdir -p /etc/named
hcp bRootServer/* bRootServer:/etc/named/.
hcp bRootServer/named.conf bRootServer:/etc/.

###Deploiement de la machine cRootServer###
himage cRootServer mkdir -p /etc/named
hcp cRootServer/* cRootServer:/etc/named/.
hcp cRootServer/named.conf cRootServer:/etc/.


###Deploiemet de la machine dza###
himage dza mkdir -p /etc/named
hcp dza/* dza:/etc/named
hcp dza/named.conf dza:/etc/.

###Deploiement de la machine dcomza###
himage dcomza mkdir -p /etc/named
hcp dcomza/* dcomza:/etc/named
hcp dcomza/named.conf dcomza:/etc/.

###Deploiement de la machine deduza###
himage deduza mkdir -p /etc/named
hcp deduza/* deduza:/etc/named
hcp deduza/named.conf deduza:/etc/.

###Deploiement de la machine drtiutre###
himage drtiutre mkdir -p /etc/named
hcp drtiutre/* drtiutre:/etc/named
hcp drtiutre/named.conf drtiutre:/etc/.

###Deploiement de la machine dre###
himage dre mkdir -p /etc/named
hcp dre/* dre:/etc/named/.
hcp dre/named.conf dre:/etc/.

###Deploiement de la machine dfacebookza###
himage dfacebookza mkdir -p /etc/named
hcp dfacebookza/* dfacebookza:/etc/named
hcp dfacebookza/named.conf dfacebookza:/etc/.

###Deploiement de la machine dfacebookza###
himage dunivza mkdir -p /etc/named
hcp dunivza/* dunivza:/etc/named
hcp dunivza/named.conf dunivza:/etc/.

###Deploiement des configurations pc pour ping###
hcp pc1/resolv.conf pc1:/etc/.

hcp pc2/resolv.conf pc2:/etc/.

hcp pc3/resolv.conf pc3:/etc/.

hcp pc4/resolv.conf pc4:/etc/.

hcp pc5/resolv.conf pc5:/etc/.

hcp pc6/resolv.conf pc6:/etc/.


###Cette ligne permet de lancer l'autre script qui démarre les serveurs DNS###
xterm -e bash /root/tp3/demarrage.sh 
