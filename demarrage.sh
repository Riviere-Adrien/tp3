#!/bin/bash

# Lancement des serveurs DNS

himage dwikiorg killall -9 named
himage dwikiorg named -c /etc/named.conf

himage diutre killall -9 named
himage diutre named -c /etc/named.conf

himage dre killall -9 named
himage dre named -c /etc/named.conf

himage dorg killall -9 named
himage dorg named -c /etc/named.conf

himage aRootServer killall -9 named
himage aRootServer named -c /etc/named.conf

himage bRootServer killall -9 named
himage bRootServer named -c /etc/named.conf

himage cRootServer killall -9 named
himage cRootServer named -c /etc/named.conf

himage dza killall -9 named
himage dza named -c /etc/named.conf

himage dcomza killall -9 named
himage dcomza named -c /etc/named.conf

himage deduza killall -9 named
himage deduza named -c /etc/named.conf

himage drtiutre killall -9 named
himage drtiutre named -c /etc/named.conf

himage dfacebookza killall -9 named
himage dfacebookza named -c /etc/named.conf

himage dunivza killall -9 named
himage dunivza named -c /etc/named.conf


